#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <algorithm>
#include <vector>

using namespace std;

/**
 *  Programming Challenge: Hangudo Man
 *  by Sean Castillo
 *  1 April 2019, but this program is no joke.
 *  Note: This program allows words with chars that aren't letters.
 *        Question Mark (?) is used to represent a covered space.
 */

const unsigned int bodyPartCount = 7; // number of incorrect guesses to game over


/**
 * For an incorrect guess, printMessage() prints the message
 */
void printMessage(unsigned int messageID)
{
    string bodyPart;
    if (messageID == 1) { bodyPart = "head";      }
    if (messageID == 2) { bodyPart = "neck";      }
    if (messageID == 3) { bodyPart = "left arm";  }
    if (messageID == 4) { bodyPart = "right arm"; }
    if (messageID == 5) { bodyPart = "spine";     }
    if (messageID == 6) { bodyPart = "left leg";  }
    if (messageID == 7) { bodyPart = "right leg"; }
    if (messageID >  7) { bodyPart = "skin cell"; } // just in case bodyPartCount > 7 ever

    cout << "Wrong guess number " << messageID << ": " << bodyPart << " hanged!" << endl;
}


/**
 * method to get words from text file and put them in a vector
 */
std::vector<string> readInWords(string textFile)
{
    std::vector<std::string> wordLines;

    ifstream wordFile;
    wordFile.open(textFile);

    // check if the input file was actually opened
    if (!wordFile.is_open())
    {
        cout << "Error: " << textFile << " not found in the directory!\n";
        return wordLines;
    }

    // check for empty file
    wordFile.seekg(0, ios_base::end);
    unsigned int len = wordFile.tellg();
    if (len == 0)
    {
        cout << "Error: " << textFile << " is empty!\n";
        return wordLines;
    }

    // reset the stream to the beginning of the file
    wordFile.seekg(0, ios_base::beg);

    // put the words into a vector
    std::string line;
    while (std::getline(wordFile, line))
    {
        wordLines.push_back(line);
    }

    // close the file
    wordFile.close();

    // export the vector
    return wordLines;

}

/**
 * method to choose a word
 */
std::string getWord(std::vector<std::string> v)
{
    if (v.empty()) { return ""; }

    srand(time(NULL));
    return v[rand() % v.size()];
}

/**
 * method to determine whether a letter was already guessed
 */
bool letterGuessed(std::vector<char> guessedLetters, char letter)
{
    for (char c : guessedLetters)
    {
        if (c == letter)
        {
            cout << "That letter was already guessed!" << endl;
            return true;
        }
    }
    return false;
}

/**
 * method to remove a guessed letter from the word vector
 * If the word does not contain that letter, return false
 */
bool removeLetterFromWord(std::vector<char> &WV, char letter)
{
    unsigned int initSize = WV.size();
    bool letterRemoved = false;

    WV.erase(std::remove( WV.begin(), WV.end(), letter), WV.end() );

    if (WV.size() < initSize) { letterRemoved = true; }

    return letterRemoved;
}

/**
 * method to put a given word into a char vector
 */
 std::vector<char> getWordVector(std::string word)
 {
    std::vector<char> v;

    // convert the word to all lower-case
    transform(word.begin(), word.end(), word.begin(), ::tolower);

    // copy that word to a char vector
    std::copy(word.begin(), word.end(), std::back_inserter(v));

     return v;
 }

 /**
  * prints the word in its current state of being unveiled
  */
 void printWordProgress(string word, std::vector<char> guessedLetters)
 {
    bool letterFound = false;
    for (char c : word)
    {
        for (char x : guessedLetters)
        {
            if (c == x) { letterFound = true; }
        }
        if (letterFound == true) { cout <<   c;}
        else                     { cout << '?';}
        letterFound = false;
    }
    cout << endl;
 }

 /**
  * method to print error messages when user gives wrong input
  * -also clears cin
  */
 void printErrorMessage(unsigned int errorNumber)
 {
    if (errorNumber == 1)
    {
        cout << "Input must consist of a single char!" << endl;
    }

    if (errorNumber == 2)
    {
        cout << "There could be an error message here!" << endl;
    }

    cin.clear();
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
 }

int main()
{
    unsigned int WGN = 0;                         // Wrong Guess Number
    string word, inputString;                     // word to guess and thing the user input
    std::vector<char> wordVector, guessedLetters; // word in char vector form
                                                  //   and vector of guessed letters
    char guessedLetter;                           // letter guessed


    // intro
    cout << "Let's play Hangudo Man. You get 7 guesses." << endl;

    // choose a word from the text file and copy it to a char vector
    word       = getWord(readInWords("wordBank.txt"));
    if (word == "")
    {
        cout << "Error: Cannot choose a word. Shutting down program." << endl;
        return -1;
    }
    wordVector = getWordVector(word);


    //for debugging; remove it during play
    //cout << "Word Chosen: " << word << endl;

    //main game loop
    while (WGN < bodyPartCount && wordVector.empty() == false)
    {
        printWordProgress(word, guessedLetters);
        cout << "Pick a letter." << endl;

        // Input Loop Label
        letterInputLabel:
        cin >> inputString;

        // Incorrect input handing
        if (inputString.length() > 1)
        {
            printErrorMessage(1); // also clears cin
            goto letterInputLabel;
        }

        // Get the letter from the inputString.
        guessedLetter = inputString[0];

        // Convert the letter to lower-case.
        guessedLetter = tolower(guessedLetter);

        // Check whether the letter was already guessed.
        if (letterGuessed(guessedLetters, guessedLetter) == false)
        {
            // Add the letter to the guessed letter list.
            guessedLetters.push_back(guessedLetter);

            // If the letter is wrong and was not chosen before, add to WGN.
            // If the letter is present, remove all instances of it.
            if (removeLetterFromWord(wordVector, guessedLetter) == false)
            {
                WGN += 1;
                printMessage(WGN);
            }
            else { cout << "CORRECT!\n"; }
        }

        //reset cin
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }


    // game over messages
    if (wordVector.empty() == true)
    {
        cout << "You win! The word is: "           << word << endl;
    }
    else
    {
        cout << "You lose! OH NO!!! The word is: " << word << endl;
    }
    return 0;
}
