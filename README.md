Programming Challenge: Hanged Man
by Sean Castillo
1 April 2019

How To Set Up:
Put a file called wordBank.txt in the directory.

wordBank.txt should have one word per line.
Any char that can be typed is allowed,
but don't use '?' because it's used as
the 'covered letter' symbol.
Avoid using non-letter symbols as much as possible.
